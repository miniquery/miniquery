// Config file based on the exemple project at https://github.com/rollup/rollup-starter-lib

import pkg from "./package.json";
import { terser } from "rollup-plugin-terser";
import cleanup from "rollup-plugin-cleanup";

export default {
  input: "src/miniQuery.js",
  plugins: [cleanup({ maxEmptyLines: 1 })],
  output: [
    {
      file: pkg.main,
      format: "cjs",
      exports: "auto",
    },
    {
      file: pkg.module,
      format: "es",
    },
    {
      file: pkg.minModule,
      format: "es",
      plugins: [terser()],
    },
    {
      file: pkg.browser,
      format: "iife",
      name: "$",
    },
    {
      file: pkg.minBrowser,
      format: "iife",
      name: "$",
      plugins: [terser()],
    },
  ],
};
