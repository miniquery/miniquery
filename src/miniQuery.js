import NodeArray from "./NodeArray.js";

/**
 * Returns an NodeArray of all Nodes matching the given selector.
 * If no parameters are given, the function will return the document
 * in a NodeArray.
 * @param {(string|Node|NodeList|Array)} [parameter=document] -
 * The selector if a string, the Node to directly include in an NodeArray,
 * a NodeList previously obtained (for exemple through querySelectorAll),
 * or an Array of Nodes (this includes NodeArrays).
 * @param {Node} [parent=document] - The parent Node from which
 * the query starts (document by default).
 * @returns {NodeArray} The list of all the DOM Nodes matching the query.
 */
function $(parameter, parent = document) {
  return NodeArray.$(parameter, parent);
}

export { $ as default };
