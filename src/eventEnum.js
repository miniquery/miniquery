// A list of the most common events, grouped by their preferred constructor
// Groups that inherit from another should be placed above their parent
// Source: https://www.w3schools.com/jsref/obj_events.asp
// * Not included because experimental as of August 2021:
// * AnimationEvent, ClipboardEvent, TransitionEvent
const eventEnum = [
  {
    eventObject: window.KeyboardEvent,
    events: ["keydown", "keypress", "keyup"],
  },
  {
    eventObject: window.DragEvent,
    events: [
      "drag",
      "dragend",
      "dragenter",
      "dragleave",
      "dragover",
      "dragstart",
      "drop",
    ],
  },
  {
    eventObject: window.WheelEvent,
    events: ["wheel"],
  },
  {
    eventObject: window.MouseEvent,
    events: [
      "click",
      "contextmenu",
      "dblclick",
      "mousedown",
      "mouseenter",
      "mouseleave",
      "mousemove",
      "mouseout",
      "mouseover",
      "mouseup",
    ],
  },
  {
    eventObject: window.FocusEvent,
    events: ["blur", "focus", "focusin", "focusout"],
  },
  {
    eventObject: window.HashChangeEvent,
    events: ["hashchange"],
  },
  {
    eventObject: window.InputEvent,
    events: ["input"],
  },
  {
    eventObject: window.PageTransitionEvent,
    events: ["pagehide", "pageshow"],
  },
  {
    eventObject: window.PopStateEvent,
    events: ["popstate"],
  },
  {
    eventObject: window.ProgressEvent,
    // The "error" type isn't present to prioritize the UIEvent version
    events: ["loadstart"],
  },
  {
    eventObject: window.StorageEvent,
    events: ["storage"],
  },
  {
    eventObject: window.TouchEvent,
    events: ["touchcancel", "touchend", "touchmove", "touchstart"],
  },
  {
    eventObject: window.UIEvent,
    events: [
      "abort",
      "beforeunload",
      "error",
      "load",
      "resize",
      "scroll",
      "select",
      "unload",
    ],
  },
  {
    eventObject: window.Event,
    events: [
      "abort",
      "afterprint",
      "beforeprint",
      "beforeunload",
      "canplay",
      "canplaythrough",
      "change",
      "error",
      "fullscreenchange",
      "fullscreenerror",
      "input",
      "invalid",
      "load",
      "loadeddata",
      "loadedmetadata",
      "message",
      "offline",
      "online",
      "open",
      "pause",
      "play",
      "playing",
      "progress",
      "ratechange",
      "resize",
      "reset",
      "scroll",
      "search",
      "seeked",
      "select",
      "show",
      "stalled",
      "submit",
      "suspend",
      "timeupdate",
      "toggle",
      "unload",
      "waiting",
    ],
  },
];

export default eventEnum;
