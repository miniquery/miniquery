import { readFileSync } from "fs";

export default ($) => {
  beforeAll(() => {
    let html = readFileSync("test/test.html").toString();
    document.documentElement.innerHTML = html;
  });

  describe("Testing the dollar function: $()", () => {
    test("Request document object with String", () => {
      expect($("document")[0]).toBe(document);
      expect($("document").length).toBe(1);
    });

    test("Request document object with empty arg", () => {
      expect($()[0]).toBe(document);
      expect($().length).toBe(1);
    });

    test("Request document object with Document object", () => {
      expect($(document)[0]).toBe(document);
      expect($(document).length).toBe(1);
    });

    test("Request body node with String", () => {
      expect($("body")[0]).toBe(document.body);
    });

    test("Request body node with Node", () => {
      let node = document.querySelector("body");
      expect($(node)[0]).toBe(document.body);
    });

    test("Request body node with NodeList", () => {
      let nodeList = document.querySelectorAll("body");
      expect($(nodeList)[0]).toBe(document.body);
    });

    test("Request second h2 from String", () => {
      let node = document.querySelectorAll("h2")[1];
      expect($("h2")[1]).toBe(node);
    });

    test("Request second h2 from Node", () => {
      let node = document.querySelectorAll("h2")[1];
      expect($(node)[0]).toBe(node);
    });

    test("Request second h2 from NodeList", () => {
      let nodeList = document.querySelectorAll("h2");
      expect($(nodeList)[1]).toBe(nodeList[1]);
    });

    test("Request second h2 from Array", () => {
      let arr = Array.from(document.querySelectorAll("h2"));
      expect($(arr)[1]).toBe(arr[1]);
    });

    test("Request h2.subtitle with div.footer parent", () => {
      let footerNode = $(".footer")[0];
      expect($("h2.subtitle", footerNode)[0].innerHTML).toBe("Footer subtitle");
    });

    test("Request div.footer > h2.subtitle from String", () => {
      expect($("div.footer h2.subtitle")[0].innerHTML).toBe("Footer subtitle");
    });

    test("Throw TypeError when invalid selector argument", () => {
      expect(() => {
        $(42);
      }).toThrowError(TypeError);
    });

    test("Throw TypeError when invalid node in Array", () => {
      expect(() => {
        $([42, 42]);
      }).toThrowError(TypeError);
    });

    test("Throw TypeError when invalid parent argument", () => {
      expect(() => {
        $("body", $("h2"));
      }).toThrowError(TypeError);
    });
  });
};
