import { readFileSync } from "fs";

export default ($) => {
  beforeEach(() => {
    let html = readFileSync("test/test.html").toString();
    document.documentElement.innerHTML = html;
  });

  afterEach(() => {
    // Shallow cleanup of the DOM
    // ! Does not fully clean the DOM, caution advised
    document.documentElement.innerHTML = "";
  });

  describe("end()", () => {
    let buttons;
    beforeEach(() => {
      buttons = $(".buttonContainer > button");
    });

    test("Is a method", () => {
      expect($().end).toBeInstanceOf(Function);
    });

    test("Returns one ancestor", () => {
      expect(buttons.eq(0).end()).toBe(buttons);
    });

    test("Returns two ancestors", () => {
      expect(buttons.eq(0).eq(1).end().end()).toBe(buttons);
    });

    test("Returns itself when empty stack", () => {
      expect(buttons.on("click", () => {}).end()).toBe(buttons);
    });
  });

  describe("eq(), first(), last()", () => {
    let buttons;

    beforeEach(() => {
      buttons = $(".buttonContainer > button");
    });

    test("Are methods", () => {
      expect($().eq).toBeInstanceOf(Function);
      expect($().first).toBeInstanceOf(Function);
      expect($().last).toBeInstanceOf(Function);
    });

    test("Positive indexes, 0-based", () => {
      for (let i = 0; i < buttons.length; i++) {
        let eqQuery = buttons.eq(i);
        expect(eqQuery.length).toBe(1);
        expect(eqQuery[0].id).toBe("btn" + (i + 1));
      }
    });

    test("Negative indexes, 1-based", () => {
      for (let i = -1; i >= -buttons.length; i--) {
        let eqQuery = buttons.eq(i);
        expect(eqQuery.length).toBe(1);
        expect(eqQuery[0].id).toBe("btn" + (buttons.length + i + 1));
      }
    });

    test("Out of bounds indexes", () => {
      let outOfBoundsEq = buttons.eq(buttons.length);
      expect(outOfBoundsEq.length).toBe(0);
      outOfBoundsEq = buttons.eq(-1 - buttons.length);
      expect(outOfBoundsEq.length).toBe(0);
    });

    test("first()", () => {
      let eqQuery = buttons.first();
      expect(eqQuery.length).toBe(1);
      expect(eqQuery[0].id).toBe("btn1");
    });

    test("last()", () => {
      let eqQuery = buttons.last();
      expect(eqQuery.length).toBe(1);
      expect(eqQuery[0].id).toBe("btn" + buttons.length);
    });

    test("Have endAncestor implementation", () => {
      expect(buttons.eq(0).end()).toBe(buttons);
      expect(buttons.first().end()).toBe(buttons);
      expect(buttons.last().end()).toBe(buttons);
    });
  });

  describe("filter()", () => {
    let buttons;
    beforeEach(() => {
      buttons = $(".buttonContainer > button");
    });

    test("Is a method", () => {
      expect($().filter).toBeInstanceOf(Function);
    });

    test("Parameter: string selector", () => {
      let query = buttons.filter("#btn1, #btn3");
      // Query should now only contain #btn1 and #btn3
      expect(query.length).toBe(2);
      expect(query[0]).toBe($("#btn1")[0]);
      expect(query[1]).toBe($("#btn3")[0]);
    });

    test("Parameter: test function", () => {
      let query = buttons.filter((el) => el.id === "btn1" || el.id === "btn3");
      expect(query.length).toBe(2);
      expect(query[0]).toBe($("#btn1")[0]);
      expect(query[1]).toBe($("#btn3")[0]);
    });

    test("Parameter: NodeArray", () => {
      let query = buttons.filter($("#btn1, #btn3"));
      expect(query.length).toBe(2);
      expect(query[0]).toBe($("#btn1")[0]);
      expect(query[1]).toBe($("#btn3")[0]);
    });

    test("Parameter: Node Array", () => {
      let filterArr = [$("#btn1")[0], $("#btn3")[0]];
      let query = buttons.filter(filterArr);
      expect(query.length).toBe(2);
      expect(query[0]).toBe($("#btn1")[0]);
      expect(query[1]).toBe($("#btn3")[0]);
    });

    test("Throw error: invalid Array", () => {
      expect(() => {
        buttons.filter([buttons[0], 42]);
      }).toThrowError(TypeError);
    });

    test("Throw error: not an Array", () => {
      expect(() => {
        buttons.filter(42);
      }).toThrowError(TypeError);
    });

    test("Has endAncestor implementation", () => {
      expect(buttons.filter("#btn1, #btn3").end()).toBe(buttons);
    });
  });

  describe("not()", () => {
    let buttons;
    beforeEach(() => {
      buttons = $(".buttonContainer > button");
    });

    test("Is a method", () => {
      expect($().not).toBeInstanceOf(Function);
    });

    test("Parameter: string selector", () => {
      let query = buttons.not("#btn1, #btn3");
      // Query should now only contain #btn2 and #btn4
      expect(query.length).toBe(2);
      expect(query[0]).toBe($("#btn2")[0]);
      expect(query[1]).toBe($("#btn4")[0]);
    });

    test("Parameter: test function", () => {
      let query = buttons.not((el) => el.id === "btn1" || el.id === "btn3");
      expect(query.length).toBe(2);
      expect(query[0]).toBe($("#btn2")[0]);
      expect(query[1]).toBe($("#btn4")[0]);
    });

    test("Parameter: NodeArray", () => {
      let query = buttons.not($("#btn1, #btn3"));
      expect(query.length).toBe(2);
      expect(query[0]).toBe($("#btn2")[0]);
      expect(query[1]).toBe($("#btn4")[0]);
    });

    test("Parameter: Node Array", () => {
      let filterArr = [$("#btn1")[0], $("#btn3")[0]];
      let query = buttons.not(filterArr);
      expect(query.length).toBe(2);
      expect(query[0]).toBe($("#btn2")[0]);
      expect(query[1]).toBe($("#btn4")[0]);
    });

    test("Throw error: invalid Array", () => {
      expect(() => {
        buttons.not([buttons[0], 42]);
      }).toThrowError(TypeError);
    });

    test("Throw error: not an Array", () => {
      expect(() => {
        buttons.not(42);
      }).toThrowError(TypeError);
    });

    test("Has endAncestor implementation", () => {
      expect(buttons.not("#btn1, #btn3").end()).toBe(buttons);
    });
  });

  describe("even(), odd()", () => {
    let buttons;
    beforeEach(() => {
      buttons = $(".buttonContainer > button");
    });

    test("Are methods", () => {
      expect($().even).toBeInstanceOf(Function);
      expect($().odd).toBeInstanceOf(Function);
    });

    test("even() filters correctly", () => {
      let query = buttons.even();
      expect(query.length).toBe(Math.ceil(buttons.length / 2));
      query.forEach((el, i) => {
        expect(el).toBe(buttons[2 * i]);
      });
    });

    test("odd() filters correctly", () => {
      let query = buttons.odd();
      expect(query.length).toBe(Math.floor(buttons.length / 2));
      query.forEach((el, i) => {
        expect(el).toBe(buttons[2 * i + 1]);
      });
    });

    test("Have endAncestor implementation", () => {
      expect(buttons.even().end()).toBe(buttons);
      expect(buttons.odd().end()).toBe(buttons);
    });
  });

  describe("add()", () => {
    let buttons;
    beforeEach(() => {
      buttons = $(".buttonContainer > button");
    });

    test("Is a method", () => {
      expect($().add).toBeInstanceOf(Function);
    });

    test("String selector", () => {
      let query = buttons.add("h2");

      buttons.forEach((el) => {
        expect(query.includes(el)).toBeTruthy();
      });
      $("h2").forEach((el) => {
        expect(query.includes(el)).toBeTruthy();
      });

      expect(query.length).toBe(buttons.length + $("h2").length);
    });

    test("String selector, with context", () => {
      let query = buttons.add("h2", $(".footer")[0]);

      buttons.forEach((el) => {
        expect(query).toContain(el);
      });
      $("h2").forEach((el) => {
        if (el.innerHTML === "Footer subtitle") {
          expect(query).toContain(el);
        } else {
          expect(query).not.toContain(el);
        }
      });

      expect(query.length).toBe(
        buttons.length + $("h2", $(".footer")[0]).length
      );
    });

    test("Add single Node", () => {
      let node = $(".footer h2")[0];
      let query = buttons.add(node);

      buttons.forEach((el) => {
        expect(query).toContain(el);
      });
      expect(query).toContain(node);

      expect(query.length).toBe(buttons.length + 1);
    });

    test("Add Array of Nodes", () => {
      let query = buttons.add($("h2"));

      buttons.forEach((el) => {
        expect(query).toContain(el);
      });
      $("h2").forEach((el) => {
        expect(query).toContain(el);
      });

      expect(query.length).toBe(buttons.length + $("h2").length);
    });

    test("Has no duplicates", () => {
      let query = buttons.add($("h2")).add($("h2")[0]);

      buttons.forEach((el) => {
        expect(query).toContain(el);
      });
      $("h2").forEach((el) => {
        expect(query).toContain(el);
      });

      expect(query.length).toBe(buttons.length + $("h2").length);
    });

    test("Has endAncestor implementation", () => {
      expect(buttons.add("document").end()).toBe(buttons);
    });

    test("Throw TypeError when invalid node in Array", () => {
      expect(() => {
        buttons.add([42]);
      }).toThrowError(TypeError);
    });
  });

  describe("parent()", () => {
    test("Is a method", () => {
      expect($().parent).toBeInstanceOf(Function);
    });

    test("Get parents, no duplicates", () => {
      let query = $("h2").parent();
      expect(query.length).toBe(2);
      expect(query).toContain($("div.main")[0]);
      expect(query).toContain($("div.footer")[0]);
    });

    test("Get correct parents with filter", () => {
      let query = $("h2").parent(".footer");
      expect(query.length).toBe(1);
      expect(query).not.toContain($("div.main")[0]);
      expect(query).toContain($("div.footer")[0]);
    });

    test("Has endAncestor implementation", () => {
      let h2 = $("h2");
      expect(h2.parent().end()).toBe(h2);
    });
  });

  describe("parents(), parentsUntil()", () => {
    let buttons;
    beforeEach(() => {
      buttons = $(".buttonContainer > button");
    });

    test("Are methods", () => {
      expect($().parents).toBeInstanceOf(Function);
      expect($().parentsUntil).toBeInstanceOf(Function);
    });

    test("parents(), parentsUntil(): no args", () => {
      let query = buttons.parents();
      let queryUntil = buttons.parentsUntil();
      let countParents = 0;
      let parent = buttons[0].parentNode;

      while (parent !== document) {
        expect(query).toContain(parent);
        expect(queryUntil).toContain(parent);
        countParents++;
        parent = parent.parentNode;
      }

      expect(query.length).toBe(countParents);
      expect(queryUntil.length).toBe(countParents);
    });

    test("parents(), parentsUntil(): with selector", () => {
      let selector = ".main";
      let query = buttons.parents(selector);
      let queryUntil = buttons.parentsUntil(undefined, selector);
      let countParents = 0;
      let parent = buttons[0].parentNode;

      while (parent !== document) {
        if (parent.matches(selector)) {
          expect(query).toContain(parent);
          expect(queryUntil).toContain(parent);
          countParents++;
        } else {
          expect(query).not.toContain(parent);
          expect(queryUntil).not.toContain(parent);
        }
        parent = parent.parentNode;
      }

      expect(query.length).toBe(countParents);
      expect(queryUntil.length).toBe(countParents);
    });

    test("parentsUntil(), with until", () => {
      let until = "body";
      let query = buttons.parentsUntil(until);
      let countParents = 0;
      let parent = buttons[0].parentNode;

      while (!parent.matches(until)) {
        expect(query).toContain(parent);
        countParents++;
        parent = parent.parentNode;
      }

      expect(query.length).toBe(countParents);

      while (parent !== document) {
        expect(query).not.toContain(parent);
        parent = parent.parentNode;
      }
    });

    test("parentsUntil(), with until and selector", () => {
      let selector = ".main";
      let until = "body";
      let query = buttons.parentsUntil(until, selector);
      let countParents = 0;
      let parent = buttons[0].parentNode;

      while (!parent.matches(until)) {
        if (parent.matches(selector)) {
          expect(query).toContain(parent);
          countParents++;
        } else {
          expect(query).not.toContain(parent);
        }
        parent = parent.parentNode;
      }

      expect(query.length).toBe(countParents);

      while (parent !== document) {
        expect(query).not.toContain(parent);
        parent = parent.parentNode;
      }
    });

    test("parentsUntil(), edge case with document", () => {
      let btnWithDoc = buttons.add(document);
      let query = btnWithDoc.parentsUntil();
      let countParents = 0;
      let parent = buttons[0].parentNode;

      while (parent !== document) {
        expect(query).toContain(parent);
        countParents++;
        parent = parent.parentNode;
      }

      // The document Node does not have any parents,
      // so the count is unchanged
      expect(query.length).toBe(countParents);
    });

    test("Have endAncestor implementation", () => {
      let h2 = $("h2");
      expect(h2.parents().end()).toBe(h2);
      expect(h2.parentsUntil().end()).toBe(h2);
    });
  });

  describe("addBack()", () => {
    let buttons;
    beforeEach(() => {
      buttons = $(".buttonContainer > button");
    });

    test("Is a method", () => {
      expect($().addBack).toBeInstanceOf(Function);
    });

    test("No ancestor: throw Error", () => {
      let query = $("h2");
      expect(() => {
        query.addBack();
      }).toThrowError(Error);
    });

    test("Default use", () => {
      let query = buttons.parent().addBack();
      expect(query.length).toBe(buttons.length + buttons.parent().length);
      buttons.forEach((el) => {
        expect(query).toContain(el);
      });
      buttons.parent().forEach((el) => {
        expect(query).toContain(el);
      });
    });

    test("With filter", () => {
      let h2 = $("h2");
      let query = h2.parent().addBack(".footer > h2");
      let footerh2 = $(".footer > h2");

      expect(query.length).toBe(h2.parent().length + footerh2.length);
      h2.parent().forEach((el) => {
        expect(query).toContain(el);
      });
      footerh2.forEach((el) => {
        expect(query).toContain(el);
      });
    });

    test("Has endAncestor implementation", () => {
      expect(buttons.parent().addBack().end().end()).toBe(buttons);
    });
  });

  describe("children()", () => {
    let container;

    beforeEach(() => {
      container = $(".textNodesWrapper");
    });

    test("Is a method", () => {
      expect($().children).toBeInstanceOf(Function);
    });

    test("Without arguments", () => {
      // Code coverage: ignore non-Elements
      container.push(new Text());

      let query = container.children();
      let children = Array.from(container[0].children);

      children.forEach((node) => {
        expect(query).toContain(node);
      });

      // Make sure it's only direct children in the result
      query.forEach((node) => {
        expect(container).toContain(node.parentNode);
        expect(node.nodeType).not.toBe(Node.TEXT_NODE);
        expect(node.nodeType).not.toBe(Node.COMMENT_NODE);
      });

      expect(query.length).toBe(children.length);
    });

    test("With selector", () => {
      let query = container.children("p");
      let children = $(".textNodesWrapper > p");

      children.forEach((node) => {
        expect(query).toContain(node);
      });

      // Make sure it's only direct children in the result
      query.forEach((node) => {
        expect(container).toContain(node.parentNode);
        expect(node.nodeType).not.toBe(Node.TEXT_NODE);
        expect(node.nodeType).not.toBe(Node.COMMENT_NODE);
      });

      expect(query.length).toBe(children.length);
    });

    test("Has endAncestor implementation", () => {
      expect(container.children().end()).toBe(container);
    });
  });

  describe("contents()", () => {
    let container;

    beforeEach(() => {
      container = $(".textNodesWrapper");
    });

    test("Is a method", () => {
      expect($().contents).toBeInstanceOf(Function);
    });

    test("Grabs every node, including text and comments", () => {
      // Code coverage: ignore non-Elements
      container.push(new Text());

      let query = container.contents();
      let cNodes = Array.from(container[0].childNodes);

      cNodes.forEach((node) => {
        expect(query).toContain(node);
      });
      query.forEach((node) => {
        expect(container).toContain(node.parentNode);
      });

      // Note: there might be implicit text Nodes between
      // consecutive non-text Nodes, this is expected behavior.
      expect(query.length).toBe(cNodes.length);
    });

    test("Has endAncestor implementation", () => {
      expect(container.contents().end()).toBe(container);
    });
  });

  describe("find()", () => {
    let container;

    beforeEach(() => {
      container = $(".textNodesWrapper");
    });

    test("Is a method", () => {
      expect($().find).toBeInstanceOf(Function);
    });

    test("With string selector", () => {
      let answer = $(".textNodesWrapper p");
      let query = container.find("p");
      expect(query.length).toBe(answer.length);
      answer.forEach((el) => {
        expect(query).toContain(el);
      });
    });

    test("With Node or NodeArray", () => {
      let answer = $(".textNodesWrapper p").last();
      let query = container.find(answer[0]);
      expect(query.length).toBe(answer.length);
      answer.forEach((el) => {
        expect(query).toContain(el);
      });
    });

    test("Has endAncestor implementation", () => {
      expect(container.find("p").end()).toBe(container);
    });

    test("Throw TypeError if invalid selector", () => {
      expect(() => {
        $("h2").find(42);
      }).toThrowError(TypeError);
    });

    test("Throw TypeError if Array selector has non-Node", () => {
      expect(() => {
        $("h2").find([42]);
      }).toThrowError(TypeError);
    });
  });

  describe("siblings()", () => {
    let buttons;

    beforeEach(() => {
      buttons = $(".buttonContainer > button");
    });

    test("Is a method", () => {
      expect($().siblings).toBeInstanceOf(Function);
    });

    test("Without arguments", () => {
      let origin = "btn2";
      let query = $("#" + origin)
        .add(new Text()) // Code coverage: ignore non-Elements
        .siblings();

      buttons
        .filter((el) => el.id !== origin)
        .forEach((node) => {
          expect(query).toContain(node);
        });

      expect(query).not.toContain($("#" + origin)[0]);

      query.forEach((node) => {
        expect(node).toBeInstanceOf(Element);
        expect(node.parentNode.classList).toContain("buttonContainer");
      });

      // Length minus text node and original sibling
      expect(query.length).toBe(buttons.length - 1);
    });

    test("With selector", () => {
      let origin = "btn2";
      let filter = "btn1";
      let query = $("#" + origin).siblings("#" + filter);

      buttons
        .filter((el) => el.id !== filter)
        .forEach((node) => {
          expect(query).not.toContain(node);
        });

      expect(query).toContain($("#" + filter)[0]);

      query.forEach((node) => {
        expect(node).toBeInstanceOf(Element);
        expect(node.parentNode.classList).toContain("buttonContainer");
      });

      expect(query.length).toBe(1);
    });

    test("Has endAncestor implementation", () => {
      expect(buttons.siblings().end()).toBe(buttons);
    });
  });

  describe("closest()", () => {
    let buttons;
    beforeEach(() => {
      buttons = $(".buttonContainer > button");
    });

    test("Is a method", () => {
      expect($().closest).toBeInstanceOf(Function);
    });

    test("String selector", () => {
      let query = buttons
        .add(".textNodesWrapper") // Adding other child of .main
        .add(new Text()) // Code coverage: non-Element Node
        .closest(".main");
      expect(query.length).toBe(1);
      expect(query[0]).toBe($(".main")[0]);
    });

    test("String selector with context", () => {
      let query = buttons
        .add(".subWrapper") // Adding other child of .main
        .closest(".textNodesWrapper", $(".main")[0]);
      expect(query.length).toBe(1);
      expect(query[0]).toBe($(".textNodesWrapper")[0]);

      query = buttons
        .add(".subWrapper") // Adding other child of .main
        .closest(".textNodesWrapper", $(".textNodesWrapper")[0]);
      expect(query.length).toBe(0);
    });

    test("Element as selector", () => {
      let query = buttons
        .add(new Text()) // Code coverage: non-Element Node
        .closest($(".main")[0]);
      expect(query.length).toBe(1);
      expect(query[0]).toBe($(".main")[0]);
    });

    test("Array as selector", () => {
      let query = buttons
        .add(new Text()) // Code coverage: non-Element Node
        .closest($(".main"));
      expect(query.length).toBe(1);
      expect(query[0]).toBe($(".main")[0]);
    });

    test("Edge case: document", () => {
      let query = buttons.closest($(document));
      expect(query.length).toBe(1);
      expect(query[0]).toBe(document);
    });

    test("Has endAncestor implementation", () => {
      expect(buttons.closest("document").end()).toBe(buttons);
    });

    test("Throw TypeError when non-Node in Array", () => {
      expect(() => {
        buttons.closest([42]);
      }).toThrowError(TypeError);
    });

    test("Throw TypeError when invalid selector", () => {
      expect(() => {
        buttons.closest(42);
      }).toThrowError(TypeError);
    });
  });

  describe("next()", () => {
    test("Is a method", () => {
      expect($().next).toBeInstanceOf(Function);
    });

    test("Without arguments", () => {
      let query = $("#btn2").add(new Text()).next();
      expect(query.length).toBe(1);
      expect(query).toContain($("#btn3")[0]);
    });

    test("With selector", () => {
      let query = $("#btn2").next("#btn3");
      expect(query.length).toBe(1);
      expect(query).toContain($("#btn3")[0]);

      query = $("#btn2").next("#btn4");
      expect(query.length).toBe(0);
    });

    test("Has endAncestor implementation", () => {
      let buttons = $(".buttonContainer > button");
      expect(buttons.next().end()).toBe(buttons);
    });
  });

  describe("nextAll(), nextUntil()", () => {
    test("Are methods", () => {
      expect($().nextUntil).toBeInstanceOf(Function);
      expect($().nextAll).toBeInstanceOf(Function);
    });

    test("nextAll()", () => {
      let query = $("#btn2").nextAll();
      expect(query.length).toBe(2);
      expect(query).toContain($("#btn3")[0]);
      expect(query).toContain($("#btn4")[0]);
    });

    test("nextAll(), with selector", () => {
      let query = $("#btn2").nextAll("#btn4");
      expect(query.length).toBe(1);
      expect(query).toContain($("#btn4")[0]);
    });

    test("nextUntil(), with until", () => {
      let query = $("#btn1").add(new Text()).nextUntil("#btn4");
      expect(query.length).toBe(2);
      expect(query).toContain($("#btn2")[0]);
      expect(query).toContain($("#btn3")[0]);
    });

    test("nextUntil(), with until and selector", () => {
      let query = $("#btn1").nextUntil("#btn4", "#btn3");
      expect(query.length).toBe(1);
      expect(query).toContain($("#btn3")[0]);
    });

    test("Have endAncestor implementation", () => {
      let buttons = $(".buttonContainer > button");
      expect(buttons.nextUntil().end()).toBe(buttons);
      expect(buttons.nextAll().end()).toBe(buttons);
    });
  });

  describe("prev()", () => {
    test("Is a method", () => {
      expect($().prev).toBeInstanceOf(Function);
    });

    test("Without arguments", () => {
      let query = $("#btn3").add(new Text()).prev();
      expect(query.length).toBe(1);
      expect(query).toContain($("#btn2")[0]);
    });

    test("With selector", () => {
      let query = $("#btn3").prev("#btn2");
      expect(query.length).toBe(1);
      expect(query).toContain($("#btn2")[0]);

      query = $("#btn3").prev("#btn1");
      expect(query.length).toBe(0);
    });

    test("Has endAncestor implementation", () => {
      let buttons = $(".buttonContainer > button");
      expect(buttons.prev().end()).toBe(buttons);
    });
  });

  describe("prevAll(), prevUntil()", () => {
    test("Are methods", () => {
      expect($().prevUntil).toBeInstanceOf(Function);
      expect($().prevAll).toBeInstanceOf(Function);
    });

    test("prevAll()", () => {
      let query = $("#btn3").prevAll();
      expect(query.length).toBe(2);
      expect(query).toContain($("#btn1")[0]);
      expect(query).toContain($("#btn2")[0]);
    });

    test("prevAll(), with selector", () => {
      let query = $("#btn3").prevAll("#btn1");
      expect(query.length).toBe(1);
      expect(query).toContain($("#btn1")[0]);
    });

    test("prevUntil(), with until", () => {
      let query = $("#btn4").add(new Text()).prevUntil("#btn1");
      expect(query.length).toBe(2);
      expect(query).toContain($("#btn2")[0]);
      expect(query).toContain($("#btn3")[0]);
    });

    test("prevUntil(), with until and selector", () => {
      let query = $("#btn4").prevUntil("#btn1", "#btn2");
      expect(query.length).toBe(1);
      expect(query).toContain($("#btn2")[0]);
    });

    test("Have endAncestor implementation", () => {
      let buttons = $(".buttonContainer > button");
      expect(buttons.prevUntil().end()).toBe(buttons);
      expect(buttons.prevAll().end()).toBe(buttons);
    });
  });

  describe("has()", () => {
    test("Is a method", () => {
      expect($().has).toBeInstanceOf(Function);
    });

    test("Parameter: Element", () => {
      let query = $(".buttonContainer, .footer").has($("#btn1")[0]);
      expect(query.length).toBe(1);
      expect(query).toContain($(".buttonContainer")[0]);
      expect(query).not.toContain($(".footer")[0]);
    });

    test("Parameter: String", () => {
      let query = $(".buttonContainer, .footer").has("#btn1");
      expect(query.length).toBe(1);
      expect(query).toContain($(".buttonContainer")[0]);
      expect(query).not.toContain($(".footer")[0]);
    });

    test("Has endAncestor implementation", () => {
      let buttons = $(".buttonContainer > button");
      expect(buttons.has("#btn1").end()).toBe(buttons);
    });

    test("Throw error: invalid parameter type", () => {
      expect(() => {
        $("button").has(42);
      }).toThrowError(TypeError);
    });
  });

  describe("is()", () => {
    let buttons;
    beforeEach(() => {
      buttons = $(".buttonContainer > button");
    });

    test("Is a method", () => {
      expect($().is).toBeInstanceOf(Function);
    });

    test("Parameter: string selector", () => {
      let query = buttons.is("#btn1, #btn3");
      expect(query).toBe(true);
      query = buttons.is(".footer");
      expect(query).toBe(false);
    });

    test("Parameter: test function", () => {
      let query = buttons.is((el) => el.id === "btn1" || el.id === "btn3");
      expect(query).toBe(true);
      query = buttons.is((el) => el.id === "btn9999");
      expect(query).toBe(false);
    });

    test("Parameter: NodeArray", () => {
      let query = buttons.is($("#btn1, #btn3"));
      expect(query).toBe(true);
      query = buttons.is($(".footer"));
      expect(query).toBe(false);
    });

    test("Parameter: Node Array", () => {
      let query = buttons.is([$("#btn1")[0], $("#btn3")[0]]);
      expect(query).toBe(true);
      query = buttons.is([$(".main")[0], $(".footer")[0]]);
      expect(query).toBe(false);
    });

    test("Throw error: invalid Array", () => {
      expect(() => {
        buttons.is([buttons[0], 42]);
      }).toThrowError(TypeError);
    });

    test("Throw error: not an Array", () => {
      expect(() => {
        buttons.is(42);
      }).toThrowError(TypeError);
    });
  });

  describe("slice()", () => {
    let div;
    let divlen;

    beforeEach(() => {
      div = $("div");
      divlen = div.length;
    });

    test("start (> 0)", () => {
      let query = div.slice(2);
      expect(query.length).toBe(divlen - 2);
    });

    test("start (> 0), end (> 0)", () => {
      let query = div.slice(2, 4);
      expect(query.length).toBe(2);
    });

    test("start (< 0)", () => {
      let query = div.slice(-2);
      expect(query.length).toBe(2);
    });

    test("start (> 0), end (< 0)", () => {
      let query = div.slice(2, -1);
      expect(query.length).toBe(divlen - 3);
    });

    test("start (< 0), end (< 0)", () => {
      let query = div.slice(-3, -1);
      expect(query.length).toBe(2);
    });

    test("Has endAncestor implementation", () => {
      expect(div.slice(1).end()).toBe(div);
    });
  });

  describe("map()", () => {
    test("Applies callback", () => {
      let buttons = $(".buttonContainer > button");
      let query = buttons.map((el) => el.id);
      expect(query.length).toBe(buttons.length);
      query.forEach((el, i) => {
        expect(el).toBe(buttons[i].id);
      });
    });

    test("Has endAncestor implementation", () => {
      let div = $("div");
      expect(div.map((el) => el.id).end()).toBe(div);
    });
  });
};
