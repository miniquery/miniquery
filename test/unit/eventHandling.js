import { readFileSync } from "fs";
import { jest, describe, test, expect } from "@jest/globals";
import eventEnum from "../../src/eventEnum.js";

export default ($) => {
  beforeEach(() => {
    let html = readFileSync("test/test.html").toString();
    document.documentElement.innerHTML = html;
  });

  afterEach(() => {
    // Shallow cleanup of the DOM
    // ! Does not fully clean the DOM, caution advised
    document.documentElement.innerHTML = "";
  });

  describe(".on() method", () => {
    const eventHandler = jest
      .fn(function (event) {
        return {
          type: event.type,
          target: event.target,
          currentTarget: event.currentTarget,
          data: event.mqData,
          thisArg: this,
        };
      })
      .mockName("eventHandler");

    beforeEach(() => {
      eventHandler.mockClear();
    });

    test("Add a click and keydown event to every h2", () => {
      $("h2").on("click keydown", eventHandler);

      $("h2").forEach((node, index) => {
        // Check if the listener was correctly registered in the Node object
        expect(node._listeners["click"][0].handler).toBe(eventHandler);
        expect(node._listeners["keydown"][0].handler).toBe(eventHandler);

        const eventSequence = ["click", "keydown", "click", "click"];

        // For each event in the sequence:
        for (let i = 0; i < eventSequence.length; i++) {
          // Trigger the event
          node.dispatchEvent(new Event(eventSequence[i], { bubbles: true }));

          // Check how many times the callback has been called
          expect(eventHandler.mock.calls.length).toBe(
            eventSequence.length * index + i + 1
          );

          // Check the callback return value
          expect(
            eventHandler.mock.results[eventSequence.length * index + i].value
          ).toMatchObject({
            type: eventSequence[i],
            target: node,
            thisArg: node,
          });
        }
      });
    });

    test("Add an eventHandler with delegation", () => {
      const clickEvent = new Event("click", { bubbles: true });

      $(".main").on("click", eventHandler, { delegated: "h2.subtitle" });

      // Node created after the .on() call, but should still trigger it
      $(".main")[0].append($(".main h2.subtitle")[0].cloneNode(true));

      // Firing an event on .main should not trigger the callback
      $(".main")[0].dispatchEvent(clickEvent);
      expect(eventHandler.mock.calls.length).toBe(0);

      // Each .subtitle clicked should trigger the callback
      // Bubbling is tested through a span inside an h2
      $(".main h2.subtitle, .testSpan").forEach((node, index) => {
        node.dispatchEvent(clickEvent);
        expect(eventHandler.mock.calls.length).toBe(index + 1);
      });
    });

    test("Pass custom data to the eventHandler", () => {
      $("h2").forEach((node, index) => {
        $(node).on("click", eventHandler, {
          mqData: { i: index, e: "click" },
        });
        node.dispatchEvent(new Event("click", { bubbles: true }));

        expect(eventHandler.mock.results[index].value.data).toEqual({
          i: index,
          e: "click",
        });
      });
    });
  });

  describe(".one() method", () => {
    test("Interpret correctly the options.once argument", () => {
      // Mocking the callback to be used for handling the event
      const eventHandler = jest
        .fn(function (event) {
          return { type: event.type, target: event.target };
        })
        .mockName("eventHandler");

      $("h2").one("click keydown", eventHandler);

      $("h2").forEach((node, index) => {
        // Check if the listener was correctly registered in the Node object
        expect(node._listeners["click"][0].handler).toBe(eventHandler);
        expect(node._listeners["keydown"][0].handler).toBe(eventHandler);

        const eventSequence = [
          "click",
          "keydown",
          "click",
          "click",
          "keydown",
          "mousedown",
        ];
        const expectedCalls = [1, 2, 2, 2, 2, 2];
        const callsPerNode = 2;
        const checkReturn = [true, true, false, false, false, false];

        // For each event in the sequence:
        for (let i = 0; i < eventSequence.length; i++) {
          // Trigger the event
          node.dispatchEvent(new Event(eventSequence[i], { bubbles: true }));

          // Check how many times the callback has been called
          expect(eventHandler.mock.calls.length).toBe(
            expectedCalls[i] + callsPerNode * index
          );

          if (checkReturn[i]) {
            // Check the callback return value
            expect(
              eventHandler.mock.results[callsPerNode * index + i].value
            ).toEqual({
              type: eventSequence[i],
              target: node,
            });

            // The mousedown event should never fire the callback
            expect(
              eventHandler.mock.results[callsPerNode * index + i].value.type
            ).not.toBe("mousedown");
          }
        }
      });
    });
    test("After execution, remove the listener from the Node's cache", () => {
      const eventHandler = jest.fn().mockName("eventHandler");

      $("h2").one("click", eventHandler);

      $("h2").forEach((node) => {
        node.dispatchEvent(new MouseEvent("click"));
      });

      // Makes sure the listener isn't shared between nodes
      expect(eventHandler.mock.calls.length).toBe($("h2").length);

      $("h2").forEach((node) => {
        expect(node._listeners["click"]).toBeUndefined();
      });

      eventHandler.mockClear();

      // Testing with delegation for code coverage
      $(".buttonContainer").one("click", eventHandler, { delegated: "button" });

      $(".buttonContainer button").forEach((node) => {
        node.dispatchEvent(new MouseEvent("click", { bubbles: true }));
      });

      // Because of delegation, this should only work once per container
      expect(eventHandler.mock.calls.length).toBe($(".buttonContainer").length);

      $(".buttonContainer").forEach((node) => {
        expect(node._listeners["click"]).toBeUndefined();
      });
    });
  });

  describe(".off() method", () => {
    let returnThis = function () {
      return this;
    };
    let handler = jest.fn(returnThis).mockName("handler");
    let handler2 = jest.fn(returnThis).mockName("handler2");

    // Regenerate the mock handlers to correctly count calls
    beforeEach(() => {
      handler.mockClear();
      handler2.mockClear();

      $("h2").on("click", handler);
      $("h2").on("keydown", handler);
      $("h2").on("click", handler2);
      $("h2").on("click", handler, { delegated: ".testSpan" });
    });

    test("Without arguments", () => {
      $("h2").off();

      $(".testSpan").forEach((node) => {
        node.dispatchEvent(new Event("click", { bubbles: true }));
      });

      expect(handler.mock.calls.length).toBe(0);

      $("h2").forEach((node) => {
        expect(node._listeners["click"]).toBeUndefined();
        expect(node._listeners["keydown"]).toBeUndefined();
      });

      expect(handler.mock.calls.length).toBe(0);
      expect(handler2.mock.calls.length).toBe(0);
    });

    test("Event argument", () => {
      $("h2").off("click");

      $(".testSpan").forEach((node) => {
        node.dispatchEvent(new Event("click", { bubbles: true }));
      });

      expect(handler.mock.calls.length).toBe(0);

      $("h2").forEach((node) => {
        node.dispatchEvent(new Event("click", { bubbles: true }));
        expect(node._listeners["click"]).toBeUndefined();
      });

      expect(handler.mock.calls.length).toBe(0);
      expect(handler2.mock.calls.length).toBe(0);

      $("h2").forEach((node, index) => {
        node.dispatchEvent(new Event("keydown", { bubbles: true }));
        expect(node._listeners["keydown"]).toBeDefined();
        expect(handler.mock.calls.length).toBe(index + 1);
      });
    });

    test("Handler argument", () => {
      $("h2").off("", handler);

      let previousCalls = 0;

      $(".testSpan").forEach((node, index) => {
        node.dispatchEvent(new Event("click", { bubbles: true }));
        // Bubbling causes 3rd case to be called
        expect(handler2.mock.calls.length).toBe(index + 1);
        previousCalls = index + 1;
        // To make sure it's not delegation, check the return value
        // The return value is set to the handler's this
        expect(handler2.mock.results[index].value).not.toBe(node);
      });

      expect(handler.mock.calls.length).toBe(0);

      $("h2").forEach((node, index) => {
        node.dispatchEvent(new Event("click", { bubbles: true }));
        node.dispatchEvent(new Event("keydown", { bubbles: true }));
        expect(node._listeners["click"]).toBeDefined();
        expect(node._listeners["keydown"]).toBeUndefined();
        expect(handler2.mock.calls.length).toBe(previousCalls + index + 1);
      });

      expect(handler.mock.calls.length).toBe(0);
    });

    test("Delegated argument", () => {
      $("h2").off("", null, { delegated: ".testSpan" });

      let previousCalls = 0;

      $(".testSpan").forEach((node, index) => {
        node.dispatchEvent(new Event("click", { bubbles: true }));
        // Bubbling causes 1st and 3rd cases to be called
        expect(handler.mock.calls.length).toBe(index + 1);
        expect(handler2.mock.calls.length).toBe(index + 1);
        previousCalls = index + 1;
      });

      $("h2").forEach((node, index) => {
        node.dispatchEvent(new Event("click", { bubbles: true }));
        node.dispatchEvent(new Event("keydown", { bubbles: true }));
        expect(node._listeners["click"]).toBeDefined();
        expect(node._listeners["keydown"]).toBeDefined();
        // Cases 1 and 2 should be called
        expect(handler.mock.calls.length).toBe(previousCalls + 2 * (index + 1));
        // Case 3 should be called
        expect(handler2.mock.calls.length).toBe(previousCalls + index + 1);
      });

      // Make sure the case 4 was never called ( = no delegation)
      // To prove it, the `this` context of the handler should never be ".testSpan"
      $(".testSpan").forEach((node) => {
        for (let result of handler.mock.results) {
          expect(result.value).not.toBe(node);
        }
      });
    });
  });

  describe(".trigger() method", () => {
    let returnEvent = function (event) {
      return event;
    };
    let clickHandler = jest.fn(returnEvent).mockName("clickHandler");

    beforeEach(() => {
      clickHandler.mockClear();
    });

    test("Trigger a series of events", () => {
      let keydownHandler = jest.fn(returnEvent).mockName("keydownHandler");
      let customHandler = jest.fn(returnEvent).mockName("customHandler");

      $("h2").on("click", clickHandler);
      $("h2").on("keydown", keydownHandler);
      $("h2").on("somecustomevent", customHandler);

      $("h2").trigger("click keydown somecustomevent");
      let expectedCalls = $("h2").length;
      expect(clickHandler.mock.calls.length).toBe(expectedCalls);
      expect(keydownHandler.mock.calls.length).toBe(expectedCalls);
      expect(customHandler.mock.calls.length).toBe(expectedCalls);
    });

    test("Bubbles and cancelable by default", () => {
      let cancelHandler = jest
        .fn(function (event) {
          event.stopPropagation();
          return event;
        })
        .mockName("cancelHandler");

      $("#btn1").on("click", clickHandler);
      // Should bubble to the .buttonContainer
      $(".buttonContainer").on("click", cancelHandler);
      // Should be canceled before bubbling to .main
      $(".main").on("click", clickHandler);

      $("#btn1").trigger("click");
      expect(clickHandler.mock.calls.length).toBe(1);
      expect(cancelHandler.mock.calls.length).toBe(1);
    });

    test("Trigger the on method", () => {
      $("#btn1")[0].onclick = clickHandler;
      $("#btn1").trigger("click");
      expect(clickHandler.mock.calls.length).toBe(1);
      $("#btn1").trigger("click", { callNative: false });
      expect(clickHandler.mock.calls.length).toBe(2);
    });

    test("Trigger the default actions when available", () => {
      let spy = jest.spyOn($("#btn1")[0], "click");
      $("#btn1").trigger("click");
      expect(spy).toHaveBeenCalled();
    });

    test("Generate the correct Event object", () => {
      for (let eventType of eventEnum) {
        // Skip the event type if the constructor isn't found
        // Necessary for now, as DragEvent isn't implemented in jsdom
        if (eventType.eventObject === undefined) continue;

        for (let i in eventType.events) {
          $("#btn1").on(eventType.events[i], clickHandler);
          $("#btn1").trigger(eventType.events[i], { callNative: false });
          $("#btn1").off(eventType.events[i], clickHandler);

          expect(clickHandler.mock.results[0].value).toBeInstanceOf(
            eventType.eventObject
          );
          clickHandler.mockClear();
        }
      }
    });

    test("Options: do not use the native method to trigger an event", () => {
      let spy = jest.spyOn($("#btn1")[0], "click");
      $("#btn1").trigger("click", { callNative: false });
      expect(spy).not.toHaveBeenCalled();
    });

    test("Options: use custom event properties", () => {
      $("#btn1").on("click", clickHandler);
      $("#btn1").trigger("click", {
        callNative: false,
        eventProps: {
          clientX: 42,
          clientY: 43,
        },
      });
      expect(clickHandler.mock.results[0].value.clientX).toBe(42);
      expect(clickHandler.mock.results[0].value.clientY).toBe(43);
    });

    test("Options: use custom event constructor", () => {
      $("#btn1").on("custommouseevent", clickHandler);
      $("#btn1").trigger("custommouseevent", {
        callNative: false,
        eventConstructor: window.MouseEvent,
        eventProps: {
          clientX: 42,
          clientY: 43,
        },
      });
      expect(clickHandler.mock.results[0].value).toBeInstanceOf(
        window.MouseEvent
      );
      expect(clickHandler.mock.results[0].value.clientX).toBe(42);
      expect(clickHandler.mock.results[0].value.clientY).toBe(43);
    });
  });

  describe(".ready() method", () => {
    test("document is in ready state when executed", () => {
      $().ready(function () {
        expect(document.readyState).not.toBe("loading");
      });
    });
  });
};
