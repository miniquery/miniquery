/*
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

export default {
  bail: true,

  collectCoverage: true,

  coveragePathIgnorePatterns: [
    "<rootDir>/node_modules/",
    "<rootDir>/test/",
    "<rootDir>/dist/",
  ],

  // Indicates which provider should be used to instrument code for coverage
  coverageProvider: "babel",

  // A list of paths to directories that Jest should use to search for files in
  roots: [],

  // The test environment that will be used for testing
  testEnvironment: "jsdom",

  // A map from regular expressions to paths to transformers
  transform: {},

  verbose: false,
};
